#
# Be sure to run `pod lib lint DCAPIManager.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DCAPIManager'
  s.version          = '0.2.9'
  s.summary          = 'DCAPIManager es una librería para llamar a los Endpoints desde swift'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
DCAPIManager es una librería echa en swift 4 que contiene el código base para realizar las llamadas a los Endpoints que se utilizan en los diferentes proyectos de DaCodes.
                       DESC

  s.homepage         = 'https://bitbucket.org/dacodesiosdevelopers/dacodesapimanager'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Pedro Iván Romero Ojeda' => 'pedro.romero@dacodes.com.mx' }
  s.source           = { :git => 'git@bitbucket.org:dacodesiosdevelopers/dacodesapimanager.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'DCAPIManager/Classes/*.swift'
  
  # s.resource_bundles = {
  #   'DCAPIManager' => ['DCAPIManager/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Alamofire', '~> 4.7'
  s.swift_version = '4.0'
end
